<?php

namespace App\Http\Controllers;

use App\product;
use Illuminate\Http\Request;

class PublicController extends Controller
{
	public function about() {
		return view('about');
    }

	public function contact() {
		return view('contact');
	}

	public function viewP( $id ) {
		$product = product::find($id);
		return view('viewP',[
			'product' => $product
		]);
	}

}
