<?php

namespace App\Http\Controllers;

use App\product;
use App\User;
use Carbon\Carbon;
use Dompdf\Dompdf;
use Exception;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('home');
    }

	public function getAddProduct() {
		return view('add-product');
	}

	public function getViewProduct() {
		$product = product::all()->sortByDesc("created_at");

		return view('viewProducts',[
			'product' => $product
		]);
	}

	public function postAddProduct( Request $request ) {

		$url = "";
			if($request->hasFile('file')){
			$filename = $request->file('file')->getClientOriginalName();
			$request->file('file')->move('uploads',$filename);
			$url = url('uploads/' . $filename);
			}

		$product = new product();
		$product->title = $request->input('title');
		$product->uses = $request->input('uses');
		$product->category = $request->input('category');
		$product->description = $request->input('description');
		$product->price = $request->input('price');
		$product->photo = $url;
		$product->save();

		return redirect('/view-products');
	}

	public function getProductReport() {
		$product = product::all();

		return view('productReport',[
			'products' => $product
		]);

	}

	public function downloadReportPDF() {
		try {
			// instantiate and use the dompdf class
			$dompdf = new Dompdf();
			$dompdf->loadHtml( $this->getProductReport() );

			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper( 'A4', 'landscape' );
			$dompdf->set_option('isRemoteEnabled', 'true');


			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			$dompdf->stream("Agbeve_Product_Report_" . Carbon::now()->toDateString());
		} catch(Exception $e){
			echo "Something went wrong. Please try again.";
		}

	}

	public function delete( $pid ) {
		product::destroy($pid);
		return redirect('/view-products');
	}


	public function getViewManagers() {
		$managers = User::where('role','Admin')->get();

		return view('viewManagers',[
			'managers' => $managers
		]);
	}

}
