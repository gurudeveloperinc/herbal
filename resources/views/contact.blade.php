@extends('layouts.home')

@section('content')


<!-- Contact -->
<section class="main-body">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6 col-md-offset-3 contact">
                <h3 align="center">Contact</h3>
                <form class="form-horizontal" method="post">
                    <fieldset>
                        <legend >Send an Email. All fields with an asterisk (*) are required.</legend>
                        <div class="form-group">
                            <label for="inputName" class="control-label label-left col-xs-4">Name *</label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" id="inputName" placeholder=" Your name" name="inputName">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label label-left col-xs-4">Email *</label>
                            <div class="col-xs-8">
                                <input type="email" class="form-control" id="email" placeholder=" Email for contact" name="email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputSubject" class="control-label label-left col-xs-4">Subject *</label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" id="inputSubject" placeholder=" Enter the subject of your message here" name="inputSubject">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputMessage" class="control-label label-left col-xs-4">Message *</label>
                            <div class="col-xs-8">
                                <textarea class="form-control" id="inputMessage" placeholder="Enter your message here" name="inputMessage"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sendToYourself" class="control-label label-left col-xs-4">Send copy to yourself</label>
                            <div class="col-xs-8">
                                <div class="checkbox">
                                    <label><input type="checkbox" id="sendToYourself" name="sendToYourself">Sends a copy of the message to the address you have supplied</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label label-left col-xs-4"></label>
                            <div class=" col-xs-8">
                                <button class="btn btn-primary validate" type="submit" aria-invalid="false">Send Email</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>

        </div>
    </div>
</section>
<!-- /Contact -->



@endsection