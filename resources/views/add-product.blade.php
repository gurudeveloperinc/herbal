@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">

                        Welcome {{Auth::user()->name}} <br>

                        <form class="col-md-12 " enctype="multipart/form-data" method="post" action="{{url('/add-product')}}">
                            {{csrf_field()}}


                             <div class="form-group">
                                 <label for="" class="col-md-4 control-label">Title</label>

                                 <div class="col-md-6">
                                     <input id="" type="text" required class="form-control" name="title" value="{{ old('title') }}" >
                                 </div>
                             </div>

                            <br>   <br>
                            <div class="form-group">
                                <label for="" class="col-md-4 control-label">Category</label>
                                <select name="category">
                                    <option>Tonic</option>
                                    <option>Powder</option>
                                    <option>Leaves</option>
                                    <option>Cream and Soap</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-md-4 control-label">Uses</label>

                                <div class="col-md-6">
                                    <textarea name="uses" class="form-control" required></textarea>
                                </div>
                                <br>   <br>
                            </div>


                            <div class="form-group">
                                <label for="" class="col-md-4 control-label">Description</label>

                                <div class="col-md-6">
                                    <textarea class="form-control" name="description" required></textarea>
                                </div>
                            </div>
                            <br>   <br>
                            <div class="form-group">
                                <label for="" class="col-md-4 control-label">Price</label>

                                <div class="col-md-6">
                                    <input type="number" class="form-control" name="price" required>
                                </div>
                            </div>
                            <br>   <br>
                            <div class="form-group col-md-12">
                                <label for="" class="col-md-4 control-label">Image</label>

                                <div class="col-md-6">

                                    <input type="file" name="file" class="form-control" required>
                                </div>
                            </div>
                            <br>   <br>

                            <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-success">Add</button>
                                <a class="btn btn-primary" href="{{url('/view-products')}}">View Products</a>

                            </div>


                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection