@extends('layouts.home')

@section('content')

    <div class="row">

        <div class="col-md-8 col-md-offset-2" align="center">


            <img src="{{$product->photo}}" class="img img-thumbnail">
            <h3>{{$product->title}}</h3>
            <h3>Description:</h3>
            <p>{{$product->description}}</p>
            <h3>Uses:</h3>

            <p>
                {{$product->uses}}
            </p>

            <h3>Category:</h3>

            <p>{{$product->category}}</p>
            <h3>Price:</h3>

            <p>GHC {{$product->price}}</p>



        </div>
    </div>


@endsection