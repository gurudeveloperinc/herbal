@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">

                        Welcome {{Auth::user()->name}} <br>
                        <a class="btn btn-success" href="{{url('/add-product')}}">Add Product</a>
                        <a class="btn btn-primary" href="{{url('/view-products')}}">View Products</a>
                        <a class="btn btn-success" href="{{url('register')}}">Add Manager</a>
                        <a class="btn btn-primary" href="{{url('/view-managers')}}">View Managers</a>
                        <a class="btn" style="background-color: darkgreen; color:white;" href="{{url('/download-report')}}">Download Product Report</a>

                        <hr>

                        <h3>List of Managers</h3>

                        <table class="table table-hover">

                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Created On</th>
                            </tr>
                            @foreach($managers as $item)

                                <tr>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->email}}</td>
                                    <td>{{$item->created_at}}</td>
                                </tr>
                            @endforeach
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection