<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Herbal/ Shop</title>

    <!-- Styles -->
    <!-- Uikit CSS -->
    <link href="{{url('assets/css/uikit.min.css')}}" rel="stylesheet">
    <link href="{{url('assets/css/slidenav.almost-flat.css')}}" rel="stylesheet">
    <link href="assets/css/slideshow.almost-flat.css" rel="stylesheet">
    <link href="assets/css/sticky.almost-flat.css" rel="stylesheet">
    <link href="assets/css/tooltip.almost-flat.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="{{url('bootstrap/css/bootstrap.css')}}" rel="stylesheet">
    <!-- Animate CSS -->
    <link href="assets/css/animate.css" rel="stylesheet" />
    <!-- Pe-icon-7-stroke Fonts -->
    <link href="assets/css/helper.css" rel="stylesheet">
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet">
    <!-- Template CSS -->
    <link href="{{url('assets/css/template.css')}}" rel="stylesheet">
    <link href="{{url('assets/color/color1.css')}}" rel="stylesheet" type="text/css" title="color1">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!-- Wrap all page content -->
<div class="body-wrapper" id="page-top">
    <!-- Top Bar -->
    <section class="hidden-xs top-bar" id="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6" id="top2">
                    <ul class="contact-info">
                        <li>
                            <i class="uk-icon-phone"></i> 0302420210
                        </li>
                        <li>
                            <i class="uk-icon-phone"></i> 0302420209
                        </li>
                        <li>
                            <i class="uk-icon-envelope"></i> <a href="mailto:contact@email.com">Agbeve@gmail.com</a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </section>
    <!-- /Top Bar -->

    <!-- Sticky Menu -->
    <div id="header-sticky-wrapper" class="sticky-wrapper" style="height: 100px;">
        <header data-uk-sticky id="header" class="header">
            <div class="container">
                <div class="row" style="position: relative;">
                    <div class="col-xs-8 col-sm-4 col-md-3" id="logo">
                        <a href="{{url('/')}}" class="logo">
                            <h1>
                                <img alt="HERBAL STORE." src="{{url('images/logo.png')}}" class="default-logo">

                            </h1>
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-8 col-md-9" id="menu">

                        <div>
                            <ul class="megamenu-parent menu-zoom hidden-xs hidden-sm hidden-md">
                                <li class="menu-item current-item active">
                                    <a href="{{url('/')}}">Home</a>

                                </li>

                                <li class="menu-item">
                                    <a href="{{url('/about')}}">About Us </a>
                                </li>

                                <li class="menu-item"><a href="{{url('/contact')}}">Contact Us</a></li>
                                <li class="menu-item">
                                    <a href="{{url('/')}}"><i class="uk-icon-shopping-cart"></i> Shop</a>
                                    <div style="width: 300px;" class="dropdown dropdown-main menu-left">
                                    </div>
                                </li>

                                @if(Auth::guest())

                                    <li class="menu-item"><a href="{{url('/login')}}">Login</a></li>


                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>
    <!-- /Sticky Menu -->


    @yield('content')

    <!-- Footer -->
    <footer id="footer" class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="column ">
                        <span class="copyright"> &copy; 2017 Agbeve. All Rights Reserved. Designed By Richard Brown</span>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer -->


</div>


<!-- Scripts placed at the end of the document so the pages load faster -->

<!-- Jquery scripts -->
<script src="{{url('assets/js/jquery.min.js')}}"></script>

<!-- Uikit scripts -->
<script src="{{url('assets/js/uikit.min.js')}}"></script>
<script src="{{url('assets/js/slideshow.min.js')}}"></script>
<script src="assets/js/slideshow-fx.min.js"></script>
<script src="assets/js/slideset.min.js"></script>
<script src="{{url('assets/js/sticky.min.js')}}"></script>
<script src="assets/js/tooltip.min.js"></script>
<script src="assets/js/parallax.min.js"></script>
<script src="assets/js/lightbox.min.js"></script>
<script src="{{url('assets/js/grid.min.js')}}"></script>

<!-- WOW scripts -->
<script src="{{url('assets/js/wow.min.js')}}"></script>
<script> new WOW().init(); </script>

<!-- Template scripts -->
<script src="assets/js/template.js"></script>

<!-- Bootstrap core JavaScript -->
<script src="{{url('bootstrap/js/bootstrap.min.js')}}"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="assets/js/ie10-viewport-bug-workaround.js"></script>


</body>

<!-- Mirrored from dhtheme.com/organicfood-html/shop.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 05 Apr 2017 19:40:05 GMT -->
</html>
