@extends('layouts.home')

@section('content')

    <div class="row" style="height: 200px;">
        <div class="col-md-12" style="padding-bottom: 20px;">
            <img style="height: 200px; width:100%;" src="{{url('images/image.jpg')}}">
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <h3>Categories</h3>
            <ul>
                <li>
                    <a href="{{url('/')}}">All</a>
                </li>
                <li>
                    <a href="{{url('/?category=tonic')}}">Tonic</a>
                </li>

                <li>
                    <a href="{{url('/?category=powder')}}">Powder</a>
                </li>
                <li>
                    <a href="{{url('/?category=leaves')}}">Leaves</a>
                </li>
                <li>
                    <a href="{{url('/?category=cream and soap')}}">Cream and soap</a>
                </li>
            </ul>
        </div>
        <div class="col-md-9" align="center">
            @foreach($products as $item)
                <div class="col-md-3" style="margin-bottom:20px;" >
                    <img src="{{$item->photo}}" class="img img-thumbnail">
                    <h3>{{$item->title}}</h3>
                    <p style="height: 100px;">{{$item->description}} <br> GHC {{$item->price}}</p>
                    <a href="{{url('/view-product/' . $item->pid)}}" class="btn btn-primary">Details</a>
                </div>
            @endforeach
        </div>

        <div align="center row">
            <div class="col-md-9 col-md-offset-5">
            <a style="font-size:30px;margin-right:20px;" href="#">< Previous page</a>         <a  style="font-size:30px;" href="#">Next page ></a>
            </div>
        </div>

    </div>


@endsection