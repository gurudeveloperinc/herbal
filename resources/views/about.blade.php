<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Meta -->
    <meta name="description" content="Organic Food ecwid eshop responsive html template, can be used for a Food, Farm, Restaurant and also for any type of business or corporate site">
    <meta name="keywords" content="organic food, eshop, ecwid, food, farm, restaurant, living healthy, boostrap, responsive, html5, css3, jquery, theme, uikit, multicolor, parallax" />
    <meta name="author" content="dhsign">
    <meta name="robots" content="index, follow" />
    <meta name="revisit-after" content="3 days" />

    <title>Herbal Store</title>

    <!-- Styles -->
    <!-- Uikit CSS -->
    <link href="assets/css/uikit.min.css" rel="stylesheet">
    <link href="assets/css/progress.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slidenav.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slideshow.almost-flat.css" rel="stylesheet">
    <link href="assets/css/sticky.almost-flat.css" rel="stylesheet">
    <link href="assets/css/tooltip.almost-flat.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Animate CSS -->
    <link href="assets/css/animate.css" rel="stylesheet" />
    <!-- Pe-icon-7-stroke Fonts -->
    <link href="assets/css/helper.css" rel="stylesheet">
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet">
    <!-- Template CSS -->
    <link href="assets/css/template.css" rel="stylesheet">
    <link href="assets/color/color1.css" rel="stylesheet" type="text/css" title="color1">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<!-- Wrap all page content -->
<div class="body-wrapper" id="page-top">
    <!-- Top Bar -->
    <section class="hidden-xs top-bar" id="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6" id="top2">
                    <ul class="contact-info">
                        <li>
                            <i class="uk-icon-phone"></i> 0302420210
                        </li>
                        <li>
                            <i class="uk-icon-phone"></i> 0302420209
                        </li>
                        <li>
                            <i class="uk-icon-envelope"></i> <a href="mailto:contact@email.com">Agbeve@gmail.com</a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </section>
    <!-- /Top Bar -->

    <!-- Sticky Menu -->
    <div id="header-sticky-wrapper" class="sticky-wrapper" style="height: 100px;">
        <header data-uk-sticky id="header" class="header">
            <div class="container">
                <div class="row" style="position: relative;">
                    <div class="col-xs-8 col-sm-4 col-md-3" id="logo">
                        <a href="index.html" class="logo">
                            <h1>
                                <img alt="HERBAL STORE." src="{{url('images/logo.png')}}" class="default-logo">
                                <img width="300" height="10" alt="Organic Food" src="{{url('images/ess.png')}}" >
                            </h1>
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-8 col-md-9" id="menu">

                        <div>
                            <ul class="megamenu-parent menu-zoom hidden-xs hidden-sm hidden-md">
                                <li class="menu-item has-child current-item active">
                                    <a href="{{url('/')}}">Home</a>

                                </li>

                                <li class="menu-item">
                                    <a href="{{url('/about')}}">About Us </a>
                                </li>

                                <li class="menu-item"><a href="{{url('/contact')}}">Contacts</a></li>
                                <li class="menu-item has-child">
                                    <a href="#"><i class="uk-icon-shopping-cart"></i> Shop</a>
                                    <div style="width: 300px;" class="dropdown dropdown-main menu-left">

                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>
    <!-- /Sticky Menu -->



    <!-- 15 Years of Experience -->
    <section class="main-body">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <article itemtype="http://schema.org/Article" itemscope="" class="item item-page">
                        <meta content="en-GB" itemprop="inLanguage">
                        <div itemprop="articleBody">
                            <div class="uk-grid">
                                <div class="uk-width-large-1-2">
                                    <p class="dropcap">

                                    Agbeve herbal is a herbal hospital and pharmaceutical company located ast Sowutuom last stop.</p>

                                    <p>Year of establishment - 1983</p>
                                    <p>Business type - Limited Liability</p>
                                </div>
                                <div class="uk-width-large-1-2">
                                    <img alt="About us" src="{{url('/images/image.jpg')}}" title="About us" class="uk-border-rounded uk-container-center">
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>
    <!-- /15 Years of Experience -->





    <!-- Footer -->
    <footer id="footer" class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="column ">
                        <span class="copyright"> &copy; 2017 Agbeve. All Rights Reserved. Designed By Richard Brown</span>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer -->

    <!-- Off Canvas Menu -->
    <div class="offcanvas-menu">
        <a class="close-offcanvas" href="#"><i class="uk-icon-remove"></i></a>
        <div class="offcanvas-inner">
            <div class="module_menu">
                <h3 class="module-title">Offcanvas Menu</h3>
                <div class="module-content">
                    <ul class="nav menu">
                        <li class="current active deeper parent">
                            <a href="index.html">Home</a>
                            <ul class="nav-child unstyled small">
                                <li><a href="index.html">Organic Kitchen Home</a></li>
                                <li><a href="organic-farm-home.html">Organic Farm Home</a></li>
                                <li><a href="landing-page-home.html">Landing Page Home</a></li>
                                <li><a href="corporate-home-page.html">Corporate Home Page</a></li>
                                <li><a href="shop.html">Ecwid Shop Home</a></li>
                            </ul>
                        </li>
                        <li class="deeper parent">
                            <a href="#">Healthy Eating</a>
                            <ul class="nav-child unstyled small">
                                <li><a href="recipes.html">Recipes</a></li>
                                <li><a href="diets.html">Diets</a></li>
                                <li><a href="organic-agricultural-land.html">Organic Agricultural Land</a></li>
                            </ul>
                        </li>
                        <li><a href="news.html">News</a></li>
                        <li class="deeper parent">
                            <a href="#">Features</a>
                            <ul class="nav-child unstyled small">
                                <li class="deeper parent">
                                    <a href="#">Pages</a>
                                    <ul class="nav-child unstyled small">
                                        <li><a href="about-us.html">About us</a></li>
                                        <li><a href="our-team.html">Our Team</a></li>
                                        <li><a href="services.html">Services</a></li>
                                        <li><a href="faq.html">FAQ</a></li>
                                        <li><a href="pricing-tables.html">Pricing Tables</a></li>
                                        <li><a href="portfolio.html">Portfolio</a></li>
                                    </ul>
                                </li>
                                <li class="deeper parent">
                                    <a href="#">Features</a>
                                    <ul class="nav-child unstyled small">
                                        <li><a href="buttons.html">Buttons</a></li>
                                        <li><a href="typography.html">Typography</a></li>
                                        <li><a href="call-to-actions.html">Call To Action</a></li>
                                        <li><a href="parallax-examples.html">Parallax Examples</a></li>
                                        <li><a href="uikit-elements.html">UI Kit Elements</a></li>
                                        <li><a href="gallery.html">Gallery</a></li>
                                    </ul>
                                </li>
                                <li class="deeper parent">
                                    <a href="#">More features</a>
                                    <ul class="nav-child unstyled small">
                                        <li><a href="block-elements.html">Block Elements</a></li>
                                        <li><a href="magnifying-glass.html">Magnifying Glass</a></li>
                                        <li><a href="comingsoon.html">Coming Soon Page</a></li>
                                        <li><a href="404-error-page.html">404 Error Page</a></li>
                                        <li><a href="charts.html">Charts</a></li>
                                        <li><a href="icons.html">Icons</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="contacts.html">Contacts</a></li>
                        <li class="deeper parent">
                            <a href="shop.html">Shop</a>
                            <ul class="nav-child unstyled small">
                                <li><a href="shop.html">Ecwid Shop Example</a></li>
                                <li><a href="shop.html#!/Salmon/p/57673142/category=15703381">Ecwid Single Product</a></li>
                                <li><a href="shop.html#!/Meat/c/15703381/offset=0&amp;sort=normal">Ecwid Product Category</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- /Off Canvas Menu -->
</div>


<!-- Scripts placed at the end of the document so the pages load faster -->

<!-- Jquery scripts -->
<script src="assets/js/jquery.min.js"></script>

<!-- Uikit scripts -->
<script src="assets/js/uikit.min.js"></script>
<script src="assets/js/slideshow.min.js"></script>
<script src="assets/js/slideshow-fx.min.js"></script>
<script src="assets/js/slideset.min.js"></script>
<script src="assets/js/sticky.min.js"></script>
<script src="assets/js/tooltip.min.js"></script>
<script src="assets/js/parallax.min.js"></script>
<script src="assets/js/lightbox.min.js"></script>
<script src="assets/js/grid.min.js"></script>

<!-- WOW scripts -->
<script src="assets/js/wow.min.js"></script>
<script> new WOW().init(); </script>

<!-- Template scripts -->
<script src="assets/js/template.js"></script>

<!-- Bootstrap core JavaScript -->
<script src="bootstrap/js/bootstrap.min.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="assets/js/ie10-viewport-bug-workaround.js"></script>


</body>

<!-- Mirrored from dhtheme.com/organicfood-html/about-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 05 Apr 2017 19:40:55 GMT -->
</html>
