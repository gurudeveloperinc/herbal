<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

	$products = \App\product::all()->sortByDesc("created_at");
	if(\Illuminate\Support\Facades\Input::has("category")){
		$products = \App\product::where('category',\Illuminate\Support\Facades\Input::get("category"))->get();
	}

	return view('welcome',['products' => $products]);
});


Route::get('/about','PublicController@about');
Route::get('/contact','PublicController@contact');
Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/add-product','HomeController@getAddProduct');
Route::get('/view-products','HomeController@getViewProduct');
Route::post('/add-product','HomeController@postAddProduct');

Route::get('/view-managers','HomeController@getViewManagers');
Route::get('/download-report','HomeController@downloadReportPDF');

Route::get('/view-product/{id}','PublicController@viewP');
Route::get('/delete/{pid}','HomeController@delete');